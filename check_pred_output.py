import os
import pandas as pd
from sklearn.externals import joblib

# LOAD PARAM SELECTION MODEL
rel_path = os.path.dirname(os.path.abspath(__file__))
regressor_path = os.path.join(rel_path, "models/", "trained_models/", "bot_3.0_GBR.joblib")
regressor = joblib.load(regressor_path)


def pull_training_data(training_data_path="logs/data_file.csv", test=False):
    if test:
        num_rows = 1000
    else:
        num_rows = None

    # Pull data
    training_data = pd.read_csv(training_data_path, header=0, nrows=num_rows, sep="|")
    print(training_data.head())
    y = training_data["rank"]
    training_data.drop(["rank", "score", "me.id"], axis=1, inplace=True)
    X = training_data.values
    return X, y


test = False
data_path = "logs/data_file.csv"

# Split data for training
X, y = pull_training_data(test=test)

preds = regressor.predict(X)

print(preds[0:5])
print(y[0:5])
