import os

from utils import param_search
from conf import params
from bot_classes.bot_class_v1 import MyBot

from sklearn.externals import joblib


rel_path = os.path.dirname(os.path.abspath(__file__))
regressor_path = os.path.join(rel_path, "../", "models/", "trained_models/", "bot_1.0_GBR.joblib")

map_params = [2, 56, 56, 513772, 111.0, 164.0, 256886.0]


def bot_class_v1():

    map_params_list = [2, 56, 56, 513772, 111.0, 164.0, 256886.0]
    regressor = joblib.load(regressor_path)
    bot_params_list = param_search.search_best_params(regressor, map_params_list)

    bot_param_names = params.model_params.keys()
    map_param_names = ["NUMBER_OF_PLAYERS", "GAME_WIDTH", "GAME_HEIGHT", "MAP_HALITE_TOTAL", "MAP_HALITE_STDEV"
        , "HALITE_DENSITY", "HALITE_PER_PLAYER"]

    bot_params = dict(zip(bot_param_names, bot_params_list))
    map_params = dict(zip(map_param_names, map_params_list))

    bot = MyBot(bot_params, map_params)

    print(bot.params['NUMBER_OF_PLAYERS'])
    print(bot.map_params['NUMBER_OF_PLAYERS'])

if __name__ == "__main__":
    bot_class_v1()