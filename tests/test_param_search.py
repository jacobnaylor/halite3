import time, os
from utils import param_search
from sklearn.externals import joblib

rel_path = os.path.dirname(os.path.abspath(__file__))
regressor_path = os.path.join(rel_path, "../", "models/", "trained_models/", "bot_1.0_GBR.joblib")


def param_search_runtime():

    regressor = joblib.load(regressor_path)
    map_params = [2, 56, 56, 513772, 111.0, 164.0, 256886.0]

    start_time = time.time()
    best_params = param_search.search_best_params(regressor, map_params)
    elapsed_mins = (time.time() - start_time) / 60.0

    return elapsed_mins, best_params

