import json, subprocess, csv, os, random, time

GAMES_TO_PLAY = 50
games_played = 0

start_time = time.time()

if os.path.isfile("logs/data_file.csv"):
    write_mode = 'a'
else:
    write_mode = 'w'

map_cond_names = ['me.id', 'NUMBER_OF_PLAYERS', 'GAME_WIDTH', 'GAME_HEIGHT', 'MAP_HALITE_TOTAL', 'MAP_HALITE_STDEV'
              , 'HALITE_DENSITY', 'HALITE_PER_PLAYER', 'START_TARGET_SHIPS_AMOUNT', 'TARGET_SHIPS_RATIO', 'TARGET_RANGE'
              , 'BLOCK_BASE_ATTEMPTS_LIMIT', 'MAX_DROPOFFS', 'IDLE_LIMIT', 'LOW_HALITE_FRACTION', 'FULL_SHIP_FRACTION'
              , 'END_GAME_TURNS', 'STOP_BUILDING_SHIPS_TURN', 'STOP_BUILDING_DROPOFFS_TURN'
              , 'SHIPS_BEFORE_DROPOFF', 'DISTANCE_COST']

while games_played < GAMES_TO_PLAY:

    # Wipe out single game logs file
    log_name = f"logs/single_game_files/{int(time.time()*1000)}_{random.randint(0,5000)}.txt"
    with open(log_name, "w") as f:
        f.write("|".join(map_cond_names)+"\n")

    command = "./halite --replay-directory ./replays/ --results-as-json".split()
    command.append(f"python3 MyBot.py --log_file_path {log_name}")
    command.append(f"python3 files_to_zip/MyBot.py")
    if random.random() > 0.3333:
        command.append(f"python3 files_to_zip/MyBot.py")
        command.append(f"python3 files_to_zip/MyBot.py")

    results = subprocess.check_output(command)
    results = json.loads(results)


    with open("logs/data_file.csv", write_mode) as data:
        writer = csv.writer(data, delimiter="|")
        # Read Results of Single Game Log File:
        with open(log_name, "r") as f:
            reader = csv.reader(f, delimiter="|")
            for line in reader:
                # If line is the header, modify and write header
                if line[0] == map_cond_names[0]:
                    if write_mode == 'w':
                        line.insert(0, 'score')
                        line.insert(0, 'rank')
                        writer.writerow(line)
                        write_mode = 'a'
                    else:
                        pass
                else:
                    # Modify & write out data row
                    player_id = str(line[0])
                    try:
                        line.insert(0, results['stats'][player_id]['score'])
                    except KeyError:
                        print(results['stats'])
                        raise
                    line.insert(0, results['stats'][player_id]['rank'])
                    writer.writerow(line)

    games_played += 1
    if games_played % 10 == 0:
        games_remaining = GAMES_TO_PLAY - games_played
        elapsed_mins = (time.time() - start_time) / 60.0
        games_per_min = games_played / elapsed_mins
        mins_remaining = games_remaining / games_per_min

        print("Played %i Games of %i. Est %i Mins Remaining" % (games_played, GAMES_TO_PLAY, mins_remaining))
# print(f"Player stats from match:\n{results['stats']}")
