import pandas as pd
from sklearn.externals import joblib
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import RandomForestRegressor


def pull_training_data(training_data_path="logs/data_file.csv", test=False):
    if test:
        num_rows = 1000
    else:
        num_rows = None

    # Pull data
    training_data = pd.read_csv(training_data_path, header=0, nrows=num_rows, sep="|")
    y = training_data["rank"]
    training_data.drop(["rank", "score", "me.id"], axis=1, inplace=True)
    X = training_data.values
    return X, y


test = False
data_path = "logs/data_file.csv"

X, y = pull_training_data(test=test)

estimator = RandomForestRegressor()
parameters = {"n_estimators": [1000, 1500], "max_depth": [8, 16, 24]}

regressor = GridSearchCV(estimator, parameters, n_jobs=3, cv=3, refit=True)
regressor.fit(X, y)
print(regressor.best_score_)
print(regressor.best_params_)
best_estimator = regressor.best_estimator_

print(best_estimator.n_features_)
print(best_estimator.feature_importances_)

joblib.dump(best_estimator, 'models/trained_models/bot_1.0_RF.joblib')
# regressor = joblib.load('filename.joblib')