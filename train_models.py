import pandas as pd
from math import sqrt
from models.model_objects import estimators, parse_results

from sklearn import preprocessing
from sklearn.impute import SimpleImputer
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.pipeline import Pipeline
from sklearn.externals import joblib


def pull_training_data(training_data_path="logs/data_file.csv", test=False):
    if test:
        num_rows = 1000
    else:
        num_rows = None

    # Pull data
    training_data = pd.read_csv(training_data_path, header=0, nrows=num_rows, sep="|")
    y = training_data["rank"]
    training_data.drop(["rank", "score", "me.id"], axis=1, inplace=True)
    X = training_data.values
    return X, y


test = False
data_path = "logs/data_file.csv"

# Split data for training
X, y = pull_training_data(test=test)
X_train, _, y_train, _ = train_test_split(X, y, test_size=0, random_state=42, shuffle=True)
num_features = X_train.shape[1]

estimator, model_params = estimators.pull_model("GBR", num_features)

param_grid = {}
# Parse model_params and add them to the parameter search
for param in model_params:
    param_name = 'estimator__' + param
    param_grid[param_name] = model_params[param]

# Define objects to Normalize Data & Impute Missing Values
scaler = preprocessing.StandardScaler()
imputer = SimpleImputer()

# Define Steps in the pipeline & build Pipeline object
steps = [('imputer', imputer)
    , ('scaler', scaler)
    , ('estimator', estimator)]
pipeline = Pipeline(steps=steps)

# Define Gridsearch & run it
search = GridSearchCV(pipeline, param_grid, scoring='neg_mean_squared_error', n_jobs=3, iid=False, cv=5,
                      return_train_score=False)
search.fit(X, y)

print(sqrt(abs(search.best_score_)))
print(search.best_params_)
best_estimator = search.best_estimator_


parsed_results = parse_results.parse_grid_search_results(search)
for key, value in parsed_results:
    print(key, value)

joblib.dump(best_estimator, 'models/trained_models/bot_3.1_GBR.joblib')
# regressor = joblib.load('filename.joblib')
