model_params = dict(
    START_TARGET_SHIPS_AMOUNT=[1, 2, 6],
    TARGET_SHIPS_RATIO=[0.9, 1.0],
    TARGET_RANGE=[10, 25, 50],
    BLOCK_BASE_ATTEMPTS_LIMIT=[0],
    MAX_DROPOFFS=[0, 1, 3],
    IDLE_LIMIT=[4, 7, 10],
    LOW_HALITE_FRACTION=[5, 10, 50],
    FULL_SHIP_FRACTION=[0.7, 0.9, 0.95],
    END_GAME_TURNS=[10, 20, 30],
    STOP_BUILDING_SHIPS_TURN=[150, 200, 250],
    STOP_BUILDING_DROPOFFS_TURN=[275],
    SHIPS_BEFORE_DROPOFF=[20],
    DISTANCE_COST=[0, 1, 5, 10, 20]
)

# Starting combos = 258048000

if __name__ == "__main__":
    combos = 1
    for param in model_params:
        combos *= len(model_params[param])

    print(combos)
