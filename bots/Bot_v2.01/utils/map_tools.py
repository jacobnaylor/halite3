import hlt
import statistics


def find_highest_value_cells(game_map, GAME_WIDTH, GAME_HEIGHT):
    highest_val_cells = []
    for x in range(0, GAME_WIDTH):
        for y in range(0, GAME_HEIGHT):
            pos = hlt.Position(x, y)
            cell = game_map[pos]
            highest_val_cells.append(cell)
    highest_val_cells = sort_cells(highest_val_cells)
    return highest_val_cells


def sort_cells(cells):
    return sorted(cells, key=lambda c: c.halite_amount, reverse=True)


def process_map_halite(game_map, GAME_WIDTH, GAME_HEIGHT):
    map_halites = []
    for x in range(0, GAME_WIDTH):
        for y in range(0, GAME_HEIGHT):
            pos = hlt.Position(x, y)
            cell = game_map[pos]
            map_halites.append(cell.halite_amount)
    map_halite_total = sum(map_halites)
    map_halite_stdev = round(statistics.stdev(map_halites), 0)
    return map_halite_total, map_halite_stdev
