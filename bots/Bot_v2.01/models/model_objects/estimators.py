import numpy as np

from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor
from sklearn.linear_model import Ridge

from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasRegressor


def pull_model(model_name, num_features):
    """
    Create an estimator object & set of parameters to tune for it.
    :param model_name: A string denoting which model type to create.
    :return: A tuple containing the model object & a dict of parameters to tune.
    """

    if model_name == "RF":
        estimator = RandomForestRegressor()
        params = {"n_estimators": [50, 100, 200, 400], "max_depth": [8, 12, 16, 24]}
        # params = {"n_estimators": [5], "max_depth": [8]}
        return estimator, params

    elif model_name == "NN":
        seed = 7
        np.random.seed(seed)
        estimator = KerasRegressor(build_fn=neural_net_model, num_features=num_features, verbose=0)
        params = {"nb_epoch": [50, 100, 200], "batch_size": [1, 4]}
        return estimator, params

    elif model_name == "LR":
        estimator = Ridge()
        params = {"alpha": [0.0, 0.01, 0.1, 1.0]}
        return estimator, params

    elif model_name == "GBR":
        estimator = GradientBoostingRegressor()
        params = {'learning_rate': [0.1], 'n_estimators': [100, 200, 300], 'subsample': [0.7, 0.9, 1.0]
            , 'min_samples_leaf': [1, 5, 10], 'max_depth': [3, 5, 7]}
        return estimator, params


def neural_net_model(num_features):
    # create model
    model = Sequential()
    model.add(Dense(60, input_dim=num_features, kernel_initializer='normal', activation='relu'))
    model.add(Dense(60, kernel_initializer='normal', activation='relu'))
    model.add(Dense(60, kernel_initializer='normal', activation='relu'))
    model.add(Dense(60, kernel_initializer='normal', activation='relu'))
    model.add(Dense(1, kernel_initializer='normal'))
    # Compile model
    model.compile(loss='mean_squared_error', optimizer='adam')
    return model

