import math


def parse_grid_search_results(search):
    search_results = {}
    for n, param in enumerate(search.cv_results_['params']):
        search_results[str(param)] = math.sqrt(abs(search.cv_results_['mean_test_score'][n]))

    sorted_results = [(key, search_results[key]) for key in
                      sorted(search_results, key=search_results.get, reverse=False)]
    return sorted_results
