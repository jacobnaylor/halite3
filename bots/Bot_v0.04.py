#!/usr/bin/env python3
# Python 3.6

# Import the Halite SDK, which will let you interact with the game.
import hlt

# This library contains constant values.
from hlt import constants

# This library contains direction metadata to better interface with the game.
from hlt.positionals import Direction

# This library allows you to generate random numbers.
import random

# Logging allows you to save messages for yourself. This is required because the regular STDOUT
#   (print statements) are reserved for the engine-bot communication.
import logging

with open("log_file.txt", mode='w') as f:
    f.write("Starting Log:\n")

""" <<<Game Begin>>> """

# This game object contains the initial game state.
game = hlt.Game()
# At this point "game" variable is populated with initial map data.
# This is a good place to do computationally expensive start-up pre-processing.
# As soon as you call "ready" function below, the 2 second per turn timer will start.
game.ready("Bot_v0.4")

# Now that your bot is initialized, save a message to yourself in the logs file with some important information.
#   Here, you logs here your id, which you can always fetch from the game object by using my_id.
logging.info("Successfully created bot! My Player ID is {}.".format(game.my_id))


def return_ship(ship, me):
    move = game_map.naive_navigate(ship, me.shipyard.position)
    return ship.move(move)


def explore_ship(ship, game_map):

    # If Current Cell Has Insufficient Resources, Move Ship
    if game_map[ship.position].halite_amount < constants.MAX_HALITE / 10:
        best_halite_amount = 0
        best_cell = None

        for position in ship.position.get_surrounding_cardinals():
            cell = game_map[position]
            # Get highest Halite Value
            if cell.halite_amount > constants.MAX_HALITE / 5 and cell.halite_amount > best_halite_amount:
                best_cell = cell
                best_halite_amount = cell.halite_amount

        # Check if best_cell is none, if so, choose randomly, if not, move towards best cell
        if best_cell is None:
            move_direction = random.choice([Direction.North, Direction.South, Direction.East, Direction.West])
            move_destination = ship.position.directional_offset(move_direction)

        else:
            move_destination = best_cell.position

        move = game_map.naive_navigate(ship, move_destination)
        return ship.move(move)
    else:
        return ship.stay_still()


ship_status = {}

""" <<<Game Loop>>> """
while True:
    # This loop handles each turn of the game. The game object changes every turn, and you refresh that state by
    #   running update_frame().
    game.update_frame()
    # You extract player metadata and the updated map metadata here for convenience.
    me = game.me
    game_map = game.game_map

    # A command queue holds all the commands you will run this turn. You build this list up and submit it at the
    #   end of the turn.
    command_queue = []

    for ship in me.get_ships():

        # Choose Ship Action
        if ship.id not in ship_status:
            ship_status[ship.id] = "exploring"

        elif ship.halite_amount >= constants.MAX_HALITE * 0.50:
            ship_status[ship.id] = "returning"

        elif ship_status[ship.id] == "returning" and ship.position == me.shipyard.position:
            ship_status[ship.id] = "exploring"

        # Add Commands to Queue
        if ship_status[ship.id] == "returning":
            command_queue.append(return_ship(ship, me))
        elif ship_status[ship.id] == "exploring":
            command = explore_ship(ship, game_map)
            command_queue.append(command)
        else:
            command_queue.append(ship.stay_still())

    # If the game is in the first 200 turns and you have enough halite, spawn a ship.
    # Don't spawn a ship if you currently have a ship at port, though - the ships will collide.
    if game.turn_number <= 200 and me.halite_amount >= constants.SHIP_COST and not game_map[me.shipyard].is_occupied:
        command_queue.append(me.shipyard.spawn())

    # Send your moves back to the game environment, ending this turn.
    game.end_turn(command_queue)

log.close()