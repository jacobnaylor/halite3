#!/usr/bin/env python3
# Python 3.6

# Import the Halite SDK, which will let you interact with the game.
import hlt

# This library contains constant values.
from hlt import constants

# This library contains direction metadata to better interface with the game.
from hlt.positionals import Direction

# This library allows you to generate random numbers.
import random

# Logging allows you to save messages for yourself. This is required because the regular STDOUT
#   (print statements) are reserved for the engine-bot communication.
import logging

with open("log_file.txt", mode='w') as f:
    f.write("Starting Log:\n")

""" <<<Game Begin>>> """

# This game object contains the initial game state.
game = hlt.Game()
game_map = game.game_map

GAME_WIDTH = game_map.width
GAME_HEIGHT = game_map.height

# Set / Calculate a bunch of constants:
bot_name = "Bot_v0.8"
ship_status = {}
ship_idle_turns = {}
global_status = {}
START_TARGET_SHIPS_AMOUNT = 4
TARGET_SHIPS_RATIO = 0.5
MAX_DROPOFFS = 1
IDLE_LIMIT = 7
LOW_HALITE_VALUE = constants.MAX_HALITE / 10
FULL_SHIP_AMOUNT = constants.MAX_HALITE * 0.80

global_status['build_dropoff'] = False
global_status['build_dropoff_in_progress'] = False


def return_ship(ship, me):
    # Determine which location to drop off at
    drop_locations = me.get_dropoffs() + [me.shipyard]
    closest_distance = 99
    best_location = me.shipyard

    for location in drop_locations:
        distance = game_map.calculate_distance(ship.position, location.position)
        if distance < closest_distance:
            closest_distance = distance
            best_location = location

    move = game_map.naive_navigate(ship, best_location.position)
    return ship.move(move)


def explore_ship(ship, game_map):

    # If Current Cell Has Insufficient Resources, Move Ship
    if game_map[ship.position].halite_amount < LOW_HALITE_VALUE:
        best_halite_amount = 0
        best_cell = None

        for position in ship.position.get_surrounding_cardinals():
            cell = game_map[position]
            # Get highest Halite Value
            if cell.halite_amount > constants.MAX_HALITE / 5 and cell.halite_amount > best_halite_amount:
                best_cell = cell
                best_halite_amount = cell.halite_amount

        # Check if best_cell is none, if so, choose randomly, if not, move towards best cell
        if best_cell is None:
            move_direction = random.choice([Direction.North, Direction.South, Direction.East, Direction.West])
            move_destination = ship.position.directional_offset(move_direction)

        else:
            move_destination = best_cell.position

        move = game_map.naive_navigate(ship, move_destination)
        return ship.move(move)
    else:
        return ship.stay_still()


def collect_toward_target(ship, target_position):
    if game_map[ship.position].halite_amount < LOW_HALITE_VALUE:
        move = game_map.naive_navigate(ship, target_position)
        return ship.move(move)
    else:
        return ship.stay_still()


def idle_override_ship(ship):
    if ship.id not in ship_idle_turns:
        ship_idle_turns[ship.id] = 0

    if command == ship.stay_still():
        ship_idle_turns[ship.id] += 1
    else:
        ship_idle_turns[ship.id] = 0

    if ship_idle_turns[ship.id] > IDLE_LIMIT:
        return random_move(ship)
    else:
        return None


def random_move(ship):
    move_direction = random.choice([Direction.North, Direction.South, Direction.East, Direction.West])
    move_destination = ship.position.directional_offset(move_direction)
    return ship.move(game_map.naive_navigate(ship, move_destination))


def at_drop_location(ship):
    drop_locations = me.get_dropoffs() + [me.shipyard]
    for location in drop_locations:
        if ship.position == location.position:
            return True
    return False


def sort_cells(cells):
    return sorted(cells, key=lambda c: c.halite_amount, reverse=True)


def find_highest_value_cells(game_map):
    highest_val_cells = []
    for x in range(0, GAME_WIDTH):
        for y in range(0, GAME_HEIGHT):
            pos = hlt.Position(x, y)
            cell = game_map[pos]
            highest_val_cells.append(cell)

    highest_val_cells = sort_cells(highest_val_cells)
    return highest_val_cells


def pick_nearby_target_cell(ship, highest_value_cells, game_map, range=20):
    for cell in highest_value_cells:
        if game_map.calculate_distance(ship.position, cell.position) <= range:
            return cell.position


highest_value_cells = find_highest_value_cells(game_map=game_map)

# At this point "game" variable is populated with initial map data.
# This is a good place to do computationally expensive start-up pre-processing.
# As soon as you call "ready" function below, the 2 second per turn timer will start.
game.ready(bot_name)

# Now that your bot is initialized, save a message to yourself in the logs file with some important information.
#   Here, you logs here your id, which you can always fetch from the game object by using my_id.
logging.info("Successfully created bot! My Player ID is {}.".format(game.my_id))


""" <<<Game Loop>>> """
while True:
    # This loop handles each turn of the game. The game object changes every turn, and you refresh that state by
    #   running update_frame().
    game.update_frame()
    # You extract player metadata and the updated map metadata here for convenience.
    me = game.me
    game_map = game.game_map

    # Re-sort our list of highest value cells
    highest_value_cells = sort_cells(highest_value_cells)

    # A command queue holds all the commands you will run this turn. You build this list up and submit it at the
    #   end of the turn.
    command_queue = []
    NUMBER_OF_SHIPS = len(me.get_ships())
    MAX_TARGET_SHIPS = NUMBER_OF_SHIPS * TARGET_SHIPS_RATIO

    for ship in me.get_ships():

        NUMBER_OF_TARGET_SHIPS = sum(1 for x in ship_status.values() if x == "targeting")

        # Check if we should build a drop_off
        if global_status['build_dropoff'] and me.halite_amount > constants.DROPOFF_COST:
            if game_map.calculate_distance(ship.position, me.shipyard.position) > 6 and not at_drop_location(ship):
                command_queue.append(ship.make_dropoff())
                global_status['build_dropoff'] = False
                global_status['build_dropoff_in_progress'] = True
                continue

        #########################
        ## CHOOSE SHIP ACTIONS ##
        #########################

        # Set New Ships to "Explore"
        if ship.id not in ship_status:
            ship_status[ship.id] = "exploring"

        # Set A Few Ships to "Target"
        elif ship_status[ship.id] != "returning" \
                and NUMBER_OF_SHIPS >= START_TARGET_SHIPS_AMOUNT \
                and NUMBER_OF_TARGET_SHIPS < MAX_TARGET_SHIPS:
            ship_status[ship.id] = "targeting"

        # Set Ship to "Return" if it Gets Full
        elif ship.halite_amount >= FULL_SHIP_AMOUNT:
            ship_status[ship.id] = "returning"

        # Once Returning Ship Drops Off, Resume "Exploring"
        elif ship_status[ship.id] == "returning" and at_drop_location(ship):
            ship_status[ship.id] = "exploring"

        # Add Commands to Queue
        if ship_status[ship.id] == "returning":
            command = return_ship(ship, me)
        elif ship_status[ship.id] == "exploring":
            command = explore_ship(ship, game_map)
        elif ship_status[ship.id] == "targeting":
            target_position = pick_nearby_target_cell(ship, highest_value_cells, game_map)
            command = collect_toward_target(ship, target_position=target_position)
        else:
            command = ship.stay_still()

        # Check if ship has been idle for too long. If so, override its command w/ a random direction.
        # This is to break gridlocks.
        override_command = idle_override_ship(ship)
        if override_command is not None:
            command = override_command

        command_queue.append(command)

    # If the game is in the first 200 turns and you have enough halite, spawn a ship.
    # Don't spawn a ship if you currently have a ship at port, though - the ships will collide.
    if game.turn_number <= 200 \
            and me.halite_amount >= constants.SHIP_COST \
            and not game_map[me.shipyard].is_occupied\
            and not global_status['build_dropoff']:
        command_queue.append(me.shipyard.spawn())

    if game.turn_number <= 220 and NUMBER_OF_SHIPS > 10 \
            and len(me.get_dropoffs()) < MAX_DROPOFFS\
            and not global_status['build_dropoff_in_progress']:
        global_status['build_dropoff'] = True
    elif game.turn_number > 220:
        global_status['build_dropoff'] = False

    # Send your moves back to the game environment, ending this turn.
    game.end_turn(command_queue)

    if global_status['build_dropoff_in_progress']:
        global_status['build_dropoff_in_progress'] = False

log.close()
