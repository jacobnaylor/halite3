#!/usr/bin/env python3
# Python 3.6

# Import the Halite SDK, which will let you interact with the game.
import hlt

# This library contains constant values.
from hlt import constants

# This library contains direction metadata to better interface with the game.
from hlt.positionals import Direction

# This library allows you to generate random numbers.
import random

# Logging allows you to save messages for yourself. This is required because the regular STDOUT
#   (print statements) are reserved for the engine-bot communication.
import logging

with open("log_file.txt", mode='w') as f:
    f.write("Starting Log:\n")

""" <<<Game Begin>>> """

# This game object contains the initial game state.
game = hlt.Game()
game_map = game.game_map

GAME_WIDTH = game_map.width
GAME_HEIGHT = game_map.height

# Set / Calculate a bunch of constants:
bot_name = "Bot_v0.13"
ship_status = {}
ship_target = {}
ship_idle_turns = {}
global_status = {}
START_TARGET_SHIPS_AMOUNT = 2
TARGET_SHIPS_RATIO = 0.8
TARGET_RANGE = 20
BLOCK_BASE_ATTEMPTS_MADE = 0
BLOCK_BASE_ATTEMPTS_LIMIT = 1
MAX_DROPOFFS = 1
IDLE_LIMIT = 7
LOW_HALITE_VALUE = constants.MAX_HALITE / 10
FULL_SHIP_AMOUNT = constants.MAX_HALITE * 0.80

global_status['build_dropoff'] = False
global_status['build_dropoff_in_progress'] = False


def return_ship(game_map, ship, drop_locations, me):
    # Determine which location to drop off at
    best_location = get_closest_entity_from_target(drop_locations, ship)

    move = naive_navigate_v2(game_map, ship, best_location.position)
    return ship.move(move)


def explore_ship(ship, game_map):

    # If Current Cell Has Insufficient Resources, Move Ship
    if game_map[ship.position].halite_amount < LOW_HALITE_VALUE:
        best_halite_amount = 0
        best_cell = None

        for position in ship.position.get_surrounding_cardinals():
            cell = game_map[position]
            # Get highest Halite Value
            if cell.halite_amount > constants.MAX_HALITE / 5 and cell.halite_amount > best_halite_amount:
                best_cell = cell
                best_halite_amount = cell.halite_amount

        # Check if best_cell is none, if so, choose randomly, if not, move towards best cell
        if best_cell is None:
            move_direction = random.choice([Direction.North, Direction.South, Direction.East, Direction.West])
            move_destination = ship.position.directional_offset(move_direction)

        else:
            move_destination = best_cell.position

        move = naive_navigate_v2(game_map, ship, move_destination)
        return ship.move(move)
    else:
        move = Direction.Still
        return ship.move(move)


def defend_dropoffs(ship, game_map, target):
    if game_map.calculate_distance(ship.position, target.position) > 1:
        move = naive_navigate_v2(game_map, ship, target.position)
    else:
        unsafe_moves = game_map.get_unsafe_moves(ship.position, target.position)
        if len(unsafe_moves) > 0:
            move = unsafe_moves[0]
        else:
            move = Direction.Still
    return ship.move(move)


def target_ship(ship, highest_value_cells, game_map):
    # If current Target is None, select one:
    if ship_target[ship.id] is None:
        # randomly pick the nth best cell in range (to avoid traffic jams)
        n_best_cell = random.choice([1, 2, 3])
        ship_target[ship.id] = pick_nearby_target_cell(ship, highest_value_cells, game_map,
                                                       range=TARGET_RANGE, n=n_best_cell)
    elif game_map[ship_target[ship.id]].halite_amount <= LOW_HALITE_VALUE:
        n_best_cell = random.choice([1, 2, 3])
        ship_target[ship.id] = pick_nearby_target_cell(ship, highest_value_cells, game_map,
                                                       range=TARGET_RANGE, n=n_best_cell)

    target_position = ship_target[ship.id]
    command = collect_toward_target(game_map, ship, target_position=target_position)
    return command


def move_to_target(game_map, ship, target_position):
    move = naive_navigate_v2(game_map, ship, target_position)
    return ship.move(move)


def collect_toward_target(game_map, ship, target_position):
    if game_map[ship.position].halite_amount < LOW_HALITE_VALUE:
        move = naive_navigate_v2(game_map, ship, target_position)
        return ship.move(move)
    else:
        return ship.stay_still()


def idle_override_ship(ship):
    if ship.id not in ship_idle_turns:
        ship_idle_turns[ship.id] = 0

    if command == ship.stay_still():
        ship_idle_turns[ship.id] += 1
    else:
        ship_idle_turns[ship.id] = 0

    rand_int = random.choice([-1, 0, 1])
    if ship_idle_turns[ship.id] > IDLE_LIMIT + rand_int:
        return random_move(game_map, ship)
    else:
        return None


def random_move(game_map, ship):
    move_direction = random.choice([Direction.North, Direction.South, Direction.East, Direction.West])
    move_destination = ship.position.directional_offset(move_direction)
    return ship.move(naive_navigate_v2(game_map, ship, move_destination))


def at_drop_location(ship):
    drop_locations = me.get_dropoffs() + [me.shipyard]
    for location in drop_locations:
        if ship.position == location.position:
            return True
    return False


def sort_cells(cells):
    return sorted(cells, key=lambda c: c.halite_amount, reverse=True)


def find_highest_value_cells(game_map):
    highest_val_cells = []
    for x in range(0, GAME_WIDTH):
        for y in range(0, GAME_HEIGHT):
            pos = hlt.Position(x, y)
            cell = game_map[pos]
            highest_val_cells.append(cell)
    highest_val_cells = sort_cells(highest_val_cells)
    return highest_val_cells


def pick_nearby_target_cell(ship, highest_value_cells, game_map, range=25, n=1):
    i = 1
    for cell in highest_value_cells:
        if game_map.calculate_distance(ship.position, cell.position) <= range:
            if i >= n:
                return cell.position
            else:
                i += 1


def check_dropoffs(drop_locations, game_map, me):
    for drop_location in drop_locations:
        occupant = game_map[drop_location.position].ship
        if occupant is not None:
            if occupant.owner != me.id:
                return drop_location
    return None


def get_closest_entity_from_target(entities, target):
    closest_distance = 99
    best_entity = None

    for entity in entities:
        distance = game_map.calculate_distance(target.position, entity.position)
        if distance < closest_distance:
            closest_distance = distance
            best_entity = entity
    return best_entity


def naive_navigate_v2(game_map, ship, destination):
    """
    A wrapper of the Game.naive_navigate function that returns a singular safe move towards the destination.
    Adding an improvement that the ship won't move if it has insufficient fuel.

    :param ship: The ship to move.
    :param destination: Ending position
    :return: A direction.
    """

    move_cost = int(game_map[ship.position].halite_amount / constants.MOVE_COST_RATIO)

    if ship.halite_amount >= move_cost:
        return game_map.naive_navigate(ship, destination)
    else:
        return Direction.Still


highest_value_cells = find_highest_value_cells(game_map=game_map)

players = game.players
NUMBER_OF_PLAYERS = len(players)
# MAX_TURNS = constants.MAX_TURNS
enemy_shipyards = []
for player_id in game.players:
    player = game.players[player_id]
    if player != game.me:
        enemy_shipyards.append(player.shipyard)

# As soon as you call "ready" function below, the 2 second per turn timer will start.
game.ready(bot_name)

# Now that your bot is initialized, save a message to yourself in the logs file with some important information.
#   Here, you logs here your id, which you can always fetch from the game object by using my_id.
logging.info("Successfully created bot! My Player ID is {}.".format(game.my_id))

""" <<<Game Loop>>> """
while True:
    # This loop handles each turn of the game. The game object changes every turn, and you refresh that state by
    #   running update_frame().
    game.update_frame()
    me = game.me
    game_map = game.game_map

    # Update / Reset a bunch of parameters
    ships_needing_orders = sorted(me.get_ships(), key=lambda x: x.id)
    drop_locations = me.get_dropoffs() + [me.shipyard]
    command_queue = []
    command = None
    build_command = None
    building_flag = False
    command_ships_queue = me.get_ships()
    NUMBER_OF_SHIPS = len(command_ships_queue)
    MAX_TARGET_SHIPS = NUMBER_OF_SHIPS * TARGET_SHIPS_RATIO

    # Re-sort our list of highest value cells
    highest_value_cells = sort_cells(highest_value_cells)

    ineligible_actions = ['defending', 'building']

    for ship in ships_needing_orders:

        NUMBER_OF_TARGET_SHIPS = sum(1 for x in ship_status.values() if x == "targeting")

        # Check if we should build a drop_off
        if global_status['build_dropoff']\
                and me.halite_amount > constants.DROPOFF_COST \
                and ship_status[ship.id] != "defending"\
                and not building_flag:
            if game_map.calculate_distance(ship.position, me.shipyard.position) > 6 and not at_drop_location(ship):
                command_queue.append(ship.make_dropoff())
                global_status['build_dropoff'] = False
                global_status['build_dropoff_in_progress'] = True
                building_flag = True
                continue

        #######################
        # CHOOSE SHIP ACTIONS #
        #######################

        # Set New Ships to "Explore"
        if ship.id not in ship_status:
            ship_target[ship.id] = None
            ship_status[ship.id] = "exploring"

        elif ship_status[ship.id] == "defending":
            if ship.position == ship_target[ship.id].position:
                ship_target[ship.id] = None
                ship_status[ship.id] = "exploring"

        # Block Enemy Base
        elif NUMBER_OF_SHIPS > 5 and BLOCK_BASE_ATTEMPTS_MADE < BLOCK_BASE_ATTEMPTS_LIMIT \
                and NUMBER_OF_PLAYERS == 2 and ship_status[ship.id] not in ["returning", "targeting", "blocking", "defending"]:
            ship_status[ship.id] = 'blocking'
            ship_target[ship.id] = None
            BLOCK_BASE_ATTEMPTS_MADE += 1

        # Set Ships to "Target"
        elif ship_status[ship.id] not in ["returning", "targeting", "blocking", "defending"] \
                and NUMBER_OF_SHIPS >= START_TARGET_SHIPS_AMOUNT \
                and NUMBER_OF_TARGET_SHIPS < MAX_TARGET_SHIPS:
            ship_status[ship.id] = "targeting"
            ship_target[ship.id] = None

        # Set Ship to "Return" if it Gets Full
        elif ship.halite_amount >= FULL_SHIP_AMOUNT and ship_status[ship.id] != "defending":
            ship_status[ship.id] = "returning"
            ship_target[ship.id] = None

        # Once Returning Ship Drops Off, Resume "Exploring"
        elif ship_status[ship.id] in ["returning", "defending"] and at_drop_location(ship):
            ship_status[ship.id] = "exploring"
            ship_target[ship.id] = None

        #########################
        # Add Commands to Queue #
        #########################
        if ship_status[ship.id] == "returning":
            command = return_ship(game_map, ship, drop_locations, me)
        elif ship_status[ship.id] == "defending":
            target = ship_target[ship.id]
            command = defend_dropoffs(ship, game_map, target)
        elif ship_status[ship.id] == "exploring":
            command = explore_ship(ship, game_map)
        elif ship_status[ship.id] == "targeting":
            command = target_ship(ship, highest_value_cells, game_map)
        elif ship_status[ship.id] == "blocking":
            if ship_target[ship.id] is None:
                ship_target[ship.id] = enemy_shipyards[0].position
            command = move_to_target(game_map, ship, ship_target[ship.id])
        else:
            command = ship.stay_still()

        # Check if ship has been idle for too long. If so, override its command w/ a random direction.
        # This is to break gridlocks.
        if ship_status[ship.id] != "defending":
            override_command = idle_override_ship(ship)
            if override_command is not None:
                command = override_command

        # If ship is moving, mark current position as safe
        if command not in [ship.stay_still(), ship.make_dropoff()]:
            game_map[ship.position].ship = None

        command_queue.append(command)

    # Check if dropoffs have been blocked, if so spawn or pick a defending ship
    blocked_dropoff = check_dropoffs(drop_locations, game_map, me)
    if blocked_dropoff is not None:
        if blocked_dropoff == me.shipyard and me.halite_amount > constants.SHIP_COST and not building_flag:
            command_queue.append(me.shipyard.spawn())
            building_flag = True
        else:
            # Get closest ship to dropoff
            closest_ship = get_closest_entity_from_target(ships_needing_orders, blocked_dropoff)
            ship_status[closest_ship.id] = "defending"
            ship_target[closest_ship.id] = blocked_dropoff

    # If the game is in the first 200 turns and you have enough halite, spawn a ship.
    # Don't spawn a ship if you currently have a ship at port, though - the ships will collide.
    if game.turn_number <= 200 \
            and me.halite_amount >= constants.SHIP_COST \
            and not game_map[me.shipyard].is_occupied\
            and not global_status['build_dropoff']\
            and not building_flag:
        command_queue.append(me.shipyard.spawn())
        building_flag = True

    if game.turn_number <= 220 and NUMBER_OF_SHIPS > 10 \
            and len(me.get_dropoffs()) < MAX_DROPOFFS\
            and not global_status['build_dropoff_in_progress']\
            and not building_flag:
        global_status['build_dropoff'] = True
        building_flag = True
    elif game.turn_number > 220:
        global_status['build_dropoff'] = False

    # Send your moves back to the game environment, ending this turn.
    game.end_turn(command_queue)

    if global_status['build_dropoff_in_progress']:
        global_status['build_dropoff_in_progress'] = False
