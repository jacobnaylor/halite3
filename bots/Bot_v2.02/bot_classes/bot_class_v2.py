# Import the Halite SDK, which will let you interact with the game.
import hlt, os, random, logging
from hlt import constants
from hlt.positionals import Direction

from utils import map_tools


class MyBot:

    def __init__(self, bot_name, bot_params, map_params, game, highest_value_cells):
        self.bot_name = bot_name

        # Instantiate parameters
        self.bot_params = bot_params

        # Calculate a few derived bot params
        self.bot_params['LOW_HALITE_VALUE'] = constants.MAX_HALITE / self.bot_params['LOW_HALITE_FRACTION']
        self.bot_params['FULL_SHIP_AMOUNT'] = constants.MAX_HALITE * self.bot_params['FULL_SHIP_FRACTION']
        self.bot_params['MAX_TARGET_SHIPS'] = 0 * self.bot_params['TARGET_SHIPS_RATIO']

        self.map_params = map_params
        self.params = {**self.map_params, **self.bot_params} # Concatenate both dicts

        # Instantiate global flags
        self.block_base_attempts_made = 0
        self.build_dropoff = False
        self.build_dropoff_in_progress = False
        self.building_flag = False

        # Attach Game to Bot & populate game attributes
        self.game = game
        self.enemy_shipyards = self.get_enemy_shipyards()
        self.drop_locations = self.game.me.get_dropoffs() + [self.game.me.shipyard]
        self.turns_remaining = constants.MAX_TURNS - self.game.turn_number
        self.my_ships = self.game.me.get_ships()
        self.number_of_ships = len(self.my_ships)
        self.number_of_target_ships = 0
        self.highest_value_cells = highest_value_cells

        # Attributes to hold ship commands
        self.ships_needing_commands = []
        self.provisional_commands = {}
        self.final_commands = []

    def game_state_update(self):
        """
        Updates game state, and associated attributes on the bot class.
        :return: Nothing.
        """
        self.game.update_frame()
        self.drop_locations = self.game.me.get_dropoffs() + [self.game.me.shipyard]
        self.turns_remaining = constants.MAX_TURNS - self.game.turn_number
        self.my_ships = sorted(self.game.me.get_ships(), key=lambda x: x.id)
        self.number_of_ships = len(self.my_ships)
        self.number_of_target_ships = sum(1 for x in self.my_ships if x.status == "targeting")
        self.params['MAX_TARGET_SHIPS'] = float(self.number_of_ships) * self.bot_params['TARGET_SHIPS_RATIO']

        logging.info("NUM SHIPS: " + str(self.number_of_ships))
        logging.info("NUM TARGET SHIPS: " + str(self.number_of_target_ships))
        logging.info("MAX TARGET SHIPS: " + str(self.params['MAX_TARGET_SHIPS']))
        logging.info("TARGET RATIO: " + str(self.params['TARGET_SHIPS_RATIO']))

        self.highest_value_cells = map_tools.sort_cells(self.highest_value_cells)

        self.building_flag = False

        self.ships_needing_commands = self.my_ships
        self.provisional_commands = {}
        self.final_commands = []

    def return_ship(self, ship):
        # Determine which location to drop off at
        move = self.naive_navigate_v2(ship, ship.target)
        return ship.move(move)

    def explore_ship(self, ship, safe_moves=True):
        game_map = self.game.game_map

        # If Current Cell Has Insufficient Resources, Move Ship
        if game_map[ship.position].halite_amount < self.params['LOW_HALITE_VALUE']:
            best_halite_amount = 0
            best_cell = None

            for position in ship.position.get_surrounding_cardinals():
                cell = game_map[position]
                # Get highest Halite Value
                if cell.halite_amount > constants.MAX_HALITE / 5 and cell.halite_amount > best_halite_amount:
                    best_cell = cell
                    best_halite_amount = cell.halite_amount

            # Check if best_cell is none, if so, choose randomly, if not, move towards best cell
            if best_cell is None:
                move_direction = random.choice([Direction.North, Direction.South, Direction.East, Direction.West])
                move_destination = ship.position.directional_offset(move_direction)

            else:
                move_destination = best_cell.position

            target_cell = game_map[move_destination]

            if safe_moves:
                move = self.naive_navigate_v2(ship, target_cell)
            else:
                move = self.get_unsafe_move(ship, target_cell)
            return move
        else:
            move = Direction.Still
            return move

    def defend_dropoffs(self, ship, safe_moves=True):
        """
        Instruct a ship to navigate to a dropoff and collide with anything there.
        :param ship: A ship selected to defend a dropoff point
        :param target: The dropoff point that needs defending
        :param safe_moves: A flag to determine whether to return collision safe moves or not.
        :return: Navigate safely to the dropoff point, then collide with the ship there
        """
        if self.game.game_map.calculate_distance(ship.position, ship.target.position) == 1 or not safe_moves:
            move = self.get_unsafe_move(ship)
        else:
            move = self.naive_navigate_v2(ship, ship.target)
        return move

    def move_to_target(self, ship, target=None, safe_moves=True):
        """
        Move a ship immediately toward a target position
        :param ship: Ship to move
        :param target: Target to navigate to.  If None, navigate toward ship's target attribute.
        :param safe_moves: A flag to determine whether to return collision safe moves or not.
        :return: A ship move toward target.
        """
        if target is None:
            target = ship.target

        if safe_moves:
            move = self.naive_navigate_v2(ship, target)
        else:
            move = self.get_unsafe_move(ship)
        return move

    def collect_toward_target(self, ship, target=None, safe_moves=True):
        """
        Move a ship toward a target position, but collect halite on the way.
        :param ship: Ship to move
        :param target: Target to navigate to.  If None, navigate toward ship's target attribute.
        :param safe_moves: A flag to determine whether to return collision safe moves or not.
        :return: A ship move toward target, or stay still to collect halite.
        """
        game_map = self.game.game_map
        if target is None:
            target = ship.target

        if game_map[ship.position].halite_amount < self.params['LOW_HALITE_VALUE']:
            if safe_moves:
                move = self.naive_navigate_v2(ship, target)
            else:
                move = self.get_unsafe_move(ship)
            return move
        else:
            return Direction.Still

    def idle_override_ship(self, ship, command):
        """
        Update # of turns ship has been idle. If idle too long, give it a random move. This helps break deadlocks.
        :param ship: Ship to move
        :param command: Current command being issued to ship.
        :return: Either a random move, or nothing.
        """
        if command == ship.stay_still():
            ship.idle_turns += 1
        else:
            ship.idle_turns = 0

        rand_int = random.choice([-1, 0, 1])
        if ship.idle_turns > self.params['IDLE_LIMIT'] + rand_int:
            return self.random_move(ship)
        else:
            return None

    def random_move(self, ship):
        """
        Give a ship a move in a random direction
        :param ship: Ship to move
        :return: A move in a random direction.
        """
        move_direction = random.choice([Direction.North, Direction.South, Direction.East, Direction.West])
        move_destination = ship.position.directional_offset(move_direction)
        target_cell = self.game.game_map[move_destination]
        return self.naive_navigate_v2(ship, target_cell)

    def get_unsafe_move(self, ship, target=None):

        game_map = self.game.game_map
        move_cost = int(game_map[ship.position].halite_amount / constants.MOVE_COST_RATIO)

        if target is None:
            target = ship.target
        unsafe_moves = self.game.game_map.get_unsafe_moves(ship.position, target.position)
        if len(unsafe_moves) > 0 and ship.halite_amount >= move_cost:
            move = unsafe_moves[0]
        else:
            move = Direction.Still
        return move

    def at_drop_location(self, ship):
        """
        Check if ship is at a drop location.
        :param ship: Ship to check.
        :return: True or False
        """

        for location in self.drop_locations:
            if ship.position == location.position:
                return True
        return False

    def pick_nearby_target_cell(self, ship, highest_value_cells, range=25, nth_best=1):
        """
        Choose a cell near a ship to target
        :param ship: Ship to check around
        :param highest_value_cells: A list of high value cells on the map.
        :param range: How far away to look.
        :param nth_best: Param to choose the best, 2nd best, etc option. This helps spread out ships a bit.
        :return: A cell position to target.
        """
        i = 1
        game_map = self.game.game_map
        for cell in highest_value_cells:
            if game_map.calculate_distance(ship.position, cell.position) <= range:
                if i >= nth_best:
                    return cell
                else:
                    i += 1

    def check_dropoffs(self):
        game_map = self.game.game_map
        me = self.game.me
        for drop_location in self.drop_locations:
            occupant = game_map[drop_location.position].ship
            if occupant is not None:
                if occupant.owner != me.id:
                    return drop_location
        return None

    def get_closest_entity_from_target(self, entities, target):
        closest_distance = 999
        best_entity = None
        game_map = self.game.game_map

        for entity in entities:
            distance = game_map.calculate_distance(target.position, entity.position)
            if distance < closest_distance:
                closest_distance = distance
                best_entity = entity
        return best_entity

    def naive_navigate_v2(self, ship, target):
        """
        A wrapper of the Game.naive_navigate function that returns a singular safe move towards the destination.
        Adding an improvement that the ship won't move if it has insufficient fuel.
        :param ship: The ship to move.
        :param target: Entity (Ship, Dropoff, Cell) to target
        :return: A direction.
        """
        game_map = self.game.game_map
        move_cost = int(game_map[ship.position].halite_amount / constants.MOVE_COST_RATIO)

        if ship.halite_amount >= move_cost:
            return game_map.naive_navigate(ship, target.position)
        else:
            return Direction.Still

    def get_enemy_shipyards(self):
        enemy_shipyards = []
        for player_id in self.game.players:
            player = self.game.players[player_id]
            if player != self.game.me:
                enemy_shipyards.append(player.shipyard)
        return enemy_shipyards

    def defend_base_commands(self):
        me = self.game.me
        # Check if dropoffs have been blocked, if so spawn or pick a defending ship
        blocked_dropoff = self.check_dropoffs()
        if blocked_dropoff is not None:
            if blocked_dropoff == me.shipyard and me.halite_amount > constants.SHIP_COST and not self.building_flag:
                self.building_flag = True
                self.final_commands.append(me.shipyard.spawn())
            else:
                # Get closest ship to dropoff & assign it to defend dropoff
                closest_ship = self.get_closest_entity_from_target(self.my_ships, blocked_dropoff)
                closest_ship.status = "defending"
                closest_ship.target = blocked_dropoff
                self.ships_needing_commands.remove(closest_ship)
                move = self.defend_dropoffs(closest_ship, blocked_dropoff)
                self.final_commands.append(closest_ship.move(move))

    def spawn_ship_commands(self):
        me = self.game.me
        if self.game.turn_number <= self.params['STOP_BUILDING_SHIPS_TURN'] \
                and me.halite_amount >= constants.SHIP_COST \
                and not self.game.game_map[me.shipyard].is_occupied\
                and not self.build_dropoff\
                and not self.building_flag:
            self.building_flag = True
            self.final_commands.append(me.shipyard.spawn())

    def set_build_dropoff_flag(self):
        """
        Check numerous conditions to see if we should build a dropoff.
        :return: Nothing - sets a flag to build (or save for) a dropoff.
        """
        if self.game.turn_number <= self.params['STOP_BUILDING_DROPOFFS_TURN'] \
                and self.number_of_ships > self.params['SHIPS_BEFORE_DROPOFF'] \
                and len(self.game.me.get_dropoffs()) < self.params['MAX_DROPOFFS'] \
                and not self.build_dropoff_in_progress \
                and not self.building_flag:
            self.build_dropoff = True

    def try_to_build_dropoff(self):
        if self.build_dropoff \
                and not self.building_flag \
                and self.game.me.halite_amount > constants.DROPOFF_COST:

            # Search for a ship at an appropriate dropoff location:
            ship = self.find_ship_far_from_dropoffs()

            if ship is not None:
                # Build the dropoff
                self.build_dropoff = False
                self.build_dropoff_in_progress = False  # TODO Check if this flag is redundant
                self.building_flag = True
                self.ships_needing_commands.remove(ship)
                self.final_commands.append(ship.make_dropoff())

    def find_ship_far_from_dropoffs(self, distance=6):
        game_map = self.game.game_map
        for ship in self.ships_needing_commands:
            for dropoff in self.drop_locations:
                if game_map.calculate_distance(ship.position, dropoff.position) > distance:
                    # Meets distance criteria, move to next check
                    pass
                else:
                    # Ship too close to a dropoff
                    break
            else:
                # All Checks Passed - Return current ship
                return ship
        else:
            # No suitable ship - Return None
            return None

    def set_ship_action(self, ship):
        if self.turns_remaining < self.params['END_GAME_TURNS']:
            ship.status = "ending"
            ship.target = self.get_closest_entity_from_target(self.drop_locations, ship)

        elif ship.status == "defending":
            # If defender reached its destination w/o being destroyed, reassign it
            if ship.position == ship.target.position:
                ship.status = "exploring"
                ship.target = None

        # Block Enemy Base
        elif self.number_of_ships > 5 and self.block_base_attempts_made < self.params['BLOCK_BASE_ATTEMPTS_LIMIT'] \
                and self.params['NUMBER_OF_PLAYERS'] == 2 \
                and ship.status not in ["returning", "targeting", "blocking", "defending"]:
            ship.status = 'blocking'
            ship.target = self.enemy_shipyards[0].position
            self.block_base_attempts_made += 1

        # Set Ships to "Target"
        elif ship.status not in ["returning", "targeting", "blocking", "defending"] \
                and self.number_of_ships >= self.params['START_TARGET_SHIPS_AMOUNT'] \
                and self.number_of_target_ships < self.params['MAX_TARGET_SHIPS']:

            # randomly pick the nth best cell in range (to avoid traffic jams)
            n_best_cell = random.choice([1, 2, 3, 4])
            ship.status = "targeting"
            ship.target = self.pick_nearby_target_cell(ship, self.highest_value_cells, range=self.params['TARGET_RANGE'], nth_best=n_best_cell)

        # Set Ship to "Return" if it Gets Full
        elif ship.halite_amount >= self.params['FULL_SHIP_AMOUNT'] and ship.status not in ["defending", "returning"]:
            ship.status = "returning"
            ship.target = self.get_closest_entity_from_target(entities=self.drop_locations, target=ship)

        # Once Returning Ship Drops Off, Resume "Exploring"
        elif ship.status in ["returning", "defending", "exploring"] and self.at_drop_location(ship):
            ship.status = "exploring"
            ship.target = None

        # Reset Targets for Targeting Ships
        elif ship.status == "targeting":
            if self.game.game_map[ship.target.position].halite_amount <= self.params['LOW_HALITE_VALUE']:
                n_best_cell = random.choice([1, 2, 3])
                ship.target = self.pick_nearby_target_cell(ship, self.highest_value_cells, range=self.params['TARGET_RANGE'], nth_best=n_best_cell)

    def find_efficient_cells(self):
        game_map = self.game.game_map
        efficient_cells = []
        for x in range(0, game_map.GAME_WIDTH):
            for y in range(0, game_map.GAME_HEIGHT):
                pos = hlt.Position(x, y)
                cell = game_map[pos]
                efficient_cells.append(cell)
                # TODO Get distance to nearest dropoff
                # TODO Figure out how to account for travel time & cost

        efficient_cells = self.sort_efficient_cells(efficient_cells)
        return efficient_cells

    def sort_efficient_cells(self, cells):
        return sorted(cells, key=lambda c: self.cell_value(c), reverse=True)

    def cell_value(self, c):
        closest_dropoff = self.get_closest_entity_from_target(self.drop_locations, c)
        distance = self.game.game_map.calculate_distance(c.position, closest_dropoff.position)
        value = c.halite_amount - distance * self.bot_params['DISTANCE_COST']
        return value
