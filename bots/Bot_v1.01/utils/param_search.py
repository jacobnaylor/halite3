from sklearn.ensemble import RandomForestRegressor
from conf import params


START_TARGET_SHIPS_AMOUNT_OPTIONS = params.model_params['START_TARGET_SHIPS_AMOUNT']
TARGET_SHIPS_RATIO_OPTIONS = params.model_params['TARGET_SHIPS_RATIO']
TARGET_RANGE_OPTIONS = params.model_params['TARGET_RANGE']
BLOCK_BASE_ATTEMPTS_LIMIT_OPTIONS = params.model_params['BLOCK_BASE_ATTEMPTS_LIMIT']
MAX_DROPOFFS_OPTIONS = params.model_params['MAX_DROPOFFS']
IDLE_LIMIT_OPTIONS = params.model_params['IDLE_LIMIT']
LOW_HALITE_FRACTION_OPTIONS = params.model_params['LOW_HALITE_FRACTION']
FULL_SHIP_FRACTION_OPTIONS = params.model_params['FULL_SHIP_FRACTION']
END_GAME_TURNS_OPTIONS = params.model_params['END_GAME_TURNS']
STOP_BUILDING_SHIPS_TURN_OPTIONS = params.model_params['STOP_BUILDING_SHIPS_TURN']
STOP_BUILDING_DROPOFFS_TURN_OPTIONS = params.model_params['STOP_BUILDING_DROPOFFS_TURN']
SHIPS_BEFORE_DROPOFF_OPTIONS = params.model_params['SHIPS_BEFORE_DROPOFF']

# print(len(START_TARGET_SHIPS_AMOUNT_OPTIONS))

total_to_search = len(START_TARGET_SHIPS_AMOUNT_OPTIONS)*len(TARGET_SHIPS_RATIO_OPTIONS)*len(TARGET_RANGE_OPTIONS)\
                  *len(BLOCK_BASE_ATTEMPTS_LIMIT_OPTIONS)*len(MAX_DROPOFFS_OPTIONS)*len(IDLE_LIMIT_OPTIONS)\
                  *len(LOW_HALITE_FRACTION_OPTIONS)*len(FULL_SHIP_FRACTION_OPTIONS)*len(END_GAME_TURNS_OPTIONS)\
                  *len(STOP_BUILDING_SHIPS_TURN_OPTIONS)*len(STOP_BUILDING_DROPOFFS_TURN_OPTIONS)\
                  *len(SHIPS_BEFORE_DROPOFF_OPTIONS)

# print(total_to_search)


def search_best_params(regressor, map_params):

    best_result = 999
    best_param_set = []

    for START_TARGET_SHIPS_AMOUNT in START_TARGET_SHIPS_AMOUNT_OPTIONS:
        for TARGET_SHIPS_RATIO in TARGET_SHIPS_RATIO_OPTIONS:
            for TARGET_RANGE in TARGET_RANGE_OPTIONS:
                for BLOCK_BASE_ATTEMPTS_LIMIT in BLOCK_BASE_ATTEMPTS_LIMIT_OPTIONS:
                    for MAX_DROPOFFS in MAX_DROPOFFS_OPTIONS:
                        for IDLE_LIMIT in IDLE_LIMIT_OPTIONS:
                            for LOW_HALITE_FRACTION in LOW_HALITE_FRACTION_OPTIONS:
                                for FULL_SHIP_FRACTION in FULL_SHIP_FRACTION_OPTIONS:
                                    for END_GAME_TURNS in END_GAME_TURNS_OPTIONS:
                                        for STOP_BUILDING_SHIPS_TURN in STOP_BUILDING_SHIPS_TURN_OPTIONS:
                                            for STOP_BUILDING_DROPOFFS_TURN in STOP_BUILDING_DROPOFFS_TURN_OPTIONS:
                                                for SHIPS_BEFORE_DROPOFF in SHIPS_BEFORE_DROPOFF_OPTIONS:
                                                    bot_params = [START_TARGET_SHIPS_AMOUNT
                                                                  , TARGET_SHIPS_RATIO
                                                                  , TARGET_RANGE
                                                                  , BLOCK_BASE_ATTEMPTS_LIMIT
                                                                  , MAX_DROPOFFS
                                                                  , IDLE_LIMIT
                                                                  , LOW_HALITE_FRACTION
                                                                  , FULL_SHIP_FRACTION
                                                                  , END_GAME_TURNS
                                                                  , STOP_BUILDING_SHIPS_TURN
                                                                  , STOP_BUILDING_DROPOFFS_TURN
                                                                  , SHIPS_BEFORE_DROPOFF]
                                                    params = map_params + bot_params
                                                    result = regressor.predict([params, ])

                                                    if result < best_result:
                                                        best_result = result
                                                        best_param_set = bot_params
    return best_param_set
