#!/usr/bin/env python3
# Python 3.6

# Import the Halite SDK, which will let you interact with the game.
import hlt, os, argparse
from hlt import constants
from hlt.positionals import Direction

from conf import params
from utils import param_search, map_tools
from bot_classes.bot_class_v1 import MyBot

import random, logging, sys

stderr = sys.stderr
sys.stderr = open("/dev/null", "w")

from sklearn.externals import joblib
from itertools import combinations

rel_path = os.path.dirname(os.path.abspath(__file__))
default_data_path = os.path.join(rel_path, "logs/", "single_game_log.txt")

parser = argparse.ArgumentParser("MyBot")
parser.add_argument("--log_file_path", help="File path for training data output", type=str
                    , required=False, default=default_data_path)
args = parser.parse_args()


# LOAD PARAM SELECTION MODEL
bot_params_list = param_search.random_params()

# Unpack Best Parameters - This is Super Unwieldy. Should refactor this into Dicts or something.
START_TARGET_SHIPS_AMOUNT,\
    TARGET_SHIPS_RATIO,\
    TARGET_RANGE,\
    BLOCK_BASE_ATTEMPTS_LIMIT,\
    MAX_DROPOFFS,\
    IDLE_LIMIT,\
    LOW_HALITE_FRACTION,\
    FULL_SHIP_FRACTION,\
    END_GAME_TURNS,\
    STOP_BUILDING_SHIPS_TURN,\
    STOP_BUILDING_DROPOFFS_TURN,\
    SHIPS_BEFORE_DROPOFF = bot_params_list

sys.stderr = stderr

""" <<<Game Begin>>> """

# This game object contains the initial game state.
game = hlt.Game()
game_map = game.game_map
me = game.me
version = "Bot_v2.00"

players = game.players
NUMBER_OF_PLAYERS = len(players)
GAME_WIDTH = game_map.width
GAME_HEIGHT = game_map.height

highest_value_cells = map_tools.find_highest_value_cells(game_map, GAME_WIDTH, GAME_HEIGHT)
MAP_HALITE_TOTAL, MAP_HALITE_STDEV = map_tools.process_map_halite(game_map, GAME_WIDTH, GAME_HEIGHT)
HALITE_DENSITY = round(MAP_HALITE_TOTAL / (GAME_WIDTH * GAME_HEIGHT), 0)
HALITE_PER_PLAYER = round(MAP_HALITE_TOTAL / NUMBER_OF_PLAYERS, 0)

bot_param_names = params.model_params.keys()
map_param_names = ["NUMBER_OF_PLAYERS", "GAME_WIDTH", "GAME_HEIGHT", "MAP_HALITE_TOTAL", "MAP_HALITE_STDEV"
    , "HALITE_DENSITY", "HALITE_PER_PLAYER"]

# Choose best model parameters
map_params_list = [NUMBER_OF_PLAYERS, GAME_WIDTH, GAME_HEIGHT, MAP_HALITE_TOTAL, MAP_HALITE_STDEV
    , HALITE_DENSITY, HALITE_PER_PLAYER]

bot_params = dict(zip(bot_param_names, bot_params_list))
map_params = dict(zip(map_param_names, map_params_list))

mybot = MyBot(bot_name=version,bot_params=bot_params, map_params=map_params, game=game, highest_value_cells=highest_value_cells)

# Dump Params into list for output to log file.
log_params = [me.id, mybot.params['NUMBER_OF_PLAYERS'], mybot.params['GAME_WIDTH'], mybot.params['GAME_HEIGHT']
    , mybot.params['MAP_HALITE_TOTAL'], mybot.params['MAP_HALITE_STDEV'], mybot.params['HALITE_DENSITY']
    , mybot.params['HALITE_PER_PLAYER'], mybot.params['START_TARGET_SHIPS_AMOUNT'], mybot.params['TARGET_SHIPS_RATIO']
    , mybot.params['TARGET_RANGE'], mybot.params['BLOCK_BASE_ATTEMPTS_LIMIT'], mybot.params['MAX_DROPOFFS']
    , mybot.params['IDLE_LIMIT'], mybot.params['LOW_HALITE_FRACTION'], mybot.params['FULL_SHIP_FRACTION']
    , mybot.params['END_GAME_TURNS'], mybot.params['STOP_BUILDING_SHIPS_TURN']
    , mybot.params['STOP_BUILDING_DROPOFFS_TURN'], mybot.params['SHIPS_BEFORE_DROPOFF']]

with open(args.log_file_path, mode='a') as f:
    f.write("|".join(map(str, log_params))+"\n")

# As soon as you call "ready" function below, the 2 second per turn timer will start.
game.ready(mybot.bot_name)
logging.info("Successfully created bot! My Player ID is {}.".format(game.my_id))

#############
# GAME LOOP #
#############
while True:
    # This loop handles each turn of the game. The game object changes every turn, and you refresh that state by
    #   running update_frame(), which is called in mybot.game_state_update().
    mybot.game_state_update()

    # Assign Commands to Defend Dropoffs
    mybot.defend_base_commands()

    # Check if it's time to build (or save for) a dropoff, if so, try to build one
    mybot.set_build_dropoff_flag()
    if mybot.build_dropoff:
        mybot.try_to_build_dropoff()

    turns_remaining = mybot.turns_remaining
    for ship in mybot.ships_needing_commands:
        mybot.set_ship_action(ship)

    # Assign Unsafe Preliminary Moves
    for ship in mybot.ships_needing_commands:

        logging.info(str(ship.id) + " " + str(ship.status))

        if ship.status in ["ending", "defending"]:
            move = mybot.defend_dropoffs(ship, safe_moves=False)
        elif ship.status == "exploring":
            move = mybot.explore_ship(ship, safe_moves=False)
        elif ship.status in ["blocking", "returning"]:
            move = mybot.move_to_target(ship, safe_moves=False)
        elif ship.status == "targeting":
            move = mybot.collect_toward_target(ship, safe_moves=False)
        else:
            move = Direction.Still

        mybot.provisional_commands[ship.id] = {"move": move, "obj": ship}  # TODO maybe make these a class attribute?

    # Resolve Ship Switching
    ships_to_resolve = list(mybot.provisional_commands.keys())
    resolved_ships = []
    ship_combos = combinations(ships_to_resolve, 2)
    for id1, id2 in ship_combos:
        # Unpack Ship Objects & Moves, then calc start & end positions
        ship1 = mybot.provisional_commands[id1]["obj"]
        ship1_move = mybot.provisional_commands[id1]["move"]
        ship1_start = ship1.position
        ship1_end = ship1.position.directional_offset(ship1_move)

        ship2 = mybot.provisional_commands[id2]["obj"]
        ship2_move = mybot.provisional_commands[id2]["move"]
        ship2_start = ship2.position
        ship2_end = ship2.position.directional_offset(ship2_move)

        # Check if ships are switching positions, if so, issue commands for each ship to final queue
        if ship1_start == ship2_end and ship1_end == ship2_start:
            mybot.final_commands.append(ship1.move(ship1_move))
            mybot.final_commands.append(ship2.move(ship2_move))
            resolved_ships.extend([id1, id2])
            ship1.idle_turns = 0
            ship2.idle_turns = 0

    # Give Safe Orders to Unresolved Ships
    unresolved_ships = [id for id in ships_to_resolve if id not in resolved_ships]
    for id in unresolved_ships:
        ship = mybot.provisional_commands[id]["obj"]

        if ship.status in ["ending", "defending"]:
            move = mybot.defend_dropoffs(ship, safe_moves=True)
        elif ship.status == "exploring":
            move = mybot.explore_ship(ship, safe_moves=True)
        elif ship.status in ["blocking", "returning"]:
            move = mybot.move_to_target(ship, safe_moves=True)
        elif ship.status == "targeting":
            move = mybot.collect_toward_target(ship, safe_moves=True)
        else:
            move = Direction.Still

        # Update Ship Inactivity
        if move == Direction.Still:
            ship.idle_turns += 1
        else:
            ship.idle_turns = 0

        rand_int = random.choice([-1, 0, 1])
        if ship.idle_turns > mybot.params['IDLE_LIMIT'] + rand_int:
            move = mybot.random_move(ship)

        # Mark Current Position as Safe
        game_map[ship.position].ship = None

        # Mark New Position as Unsafe
        game_map[ship.position.directional_offset(move)].mark_unsafe(ship)
        mybot.final_commands.append(ship.move(move))

    # Assign Build Ships Commands
    mybot.spawn_ship_commands()

    # Send your moves back to the game environment, ending this turn.
    game.end_turn(mybot.final_commands)
